#include<iostream>
#include "Piece.h"
#include<array>
#include"Board.h"

int main()
{

	Piece piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Square);

	Board ourBoard;
	std::cout << ourBoard;
	ourBoard[{0, 0}] = piece;

	std::cout << ourBoard;
	
	
	return 0;
}