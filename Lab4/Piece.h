#pragma once
#include<iostream>
#include<cstdint>

class Piece
{
public:
	enum class Body : uint8_t {
		Full,
		Hollow
	};
	enum class Color : uint8_t{
		Dark,
		Light
	};
	enum class Height : uint8_t {
		Short,
		Tall
	};
	enum class Shape : uint8_t {
		Round,
		Square
	};

	Piece(Body body, Color color, Height height, Shape shape);

	Body GetBody() const;
	Color GetColor() const;
	Height GetHeight() const;
	Shape GetShape() const;

	friend std::ostream& operator<<(std::ostream & out, const Piece& piece);
	friend std::ostream& operator<<(std::ostream& out, const Board& board);

private:
	Body m_body : 1; // Pt a reprezenta variabila in clasa, se va folosi 1 bit.
	Color m_color : 1;
	Height m_height : 1;
	Shape m_shape : 1;

};

