#include "Piece.h"

Piece::Piece(Body body, Color color, Height height, Shape shape) :
	m_body(body), m_color(color), m_height(height), m_shape(shape)
{
	static_assert(sizeof(*this)==1, "Piece size is not 1"); // verificare la compilare
	sizeof(*this); //16 bytes
}

Piece::Body Piece::GetBody() const
{
	return m_body;
}

Piece::Color Piece::GetColor() const
{
	return m_color;
}

Piece::Height Piece::GetHeight() const
{
	return m_height;
}

Piece::Shape Piece::GetShape() const
{
	return m_shape;
}

std::ostream& operator<<(std::ostream& out, const Piece& piece)
{
	//out << static_cast<int16_t>(Piece.m_body)
	switch (piece.m_body) {
	case Piece::Body::Full: {
		out << "Full";
		break;
	}
	case Piece::Body::Hollow: {
		out << "Hollow";
		break;
	}
	default: {
		throw "Invalid body type ! ";
	}
	}
}

std::ostream& operator<<(std::ostream& out, const Board& board) {
	switch (piece.m_color) {
		case Piece::Color::Dark{
		out << "Dark";
		break;
		}
		case Piece::Body::Light: {
			out << "Light";
			break;
		}
		default: {
			throw "Invalid color type ! ";
		}
	}

}
