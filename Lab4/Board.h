#pragma once
#include<array>
#include"Piece.h"
#include<optional>

class Board
{

public:
	//typedef std::pair<uint8_t, uint8_t> Position;
	using Position= std::pair<uint8_t, uint8_t>;

public:
	const std::optional<Piece>& operator[](const Position &position) const;
	std::optional<Piece>& operator[](const Position& position);

	friend::ostream& operator<<(std::ostream& out, const Board& board);

private:
	static const uint8_t columnNumber=4;
	static const uint8_t lineNumber=4;

private:
	std::array<std::array<std::optional<Piece>, columnNumber>, lineNumber> m_board;
	
};

