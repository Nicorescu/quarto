#include "Board.h"

const std::optional<Piece>& Board::operator[](const Position& position) const
{
	return m_board[position.first][position.second];
}

std::optional<Piece>& Board::operator[](const Position& position)
{
	auto& [line, column] = position; // structured binding
	return m_board[line][column];
}

::ostream& operator<<(std::ostream& out, const Board& board)
{
	for (const auto& line : board.m_board)
	{
		for (const auto& pieceOptional : line)
		{
			if (pieceOptional)
				out << pieceOptional.value();
			else
				out << "********";
			out << " ";
			/*
			if(pieceOptional.has_value())
			out << pieceOptional.value();
			*/
		}
		out << "\n";
	}
	return out;
}
